var colors = {
  light: [13, 13, 13],
  dark: [0, 0, 0]
}

var boardSize
var initSize = 0.065
var font
var quant = 'quantum entanglement'

function preload() {
  font = loadFont('ttf/RobotoMono-Medium.ttf')
}

function setup() {
  if (windowWidth >= windowHeight) {
    boardSize = windowHeight - 80
  } else {
    boardSize = windowWidth - 80
  }
  createCanvas(windowWidth, windowHeight)
}

function draw() {
  createCanvas(windowWidth, windowHeight)
  background(colors.light)
  rectMode(CENTER)
  colorMode(RGB, 255, 255, 255, 1)
  textFont(font)
  textAlign(CENTER, CENTER)

  fill(colors.dark)
  noStroke()
  rect(windowWidth *  0.5, windowHeight * 0.5, boardSize, boardSize)

  for (var k = 0; k < 3; k++) {
    push()
    translate(0, (k - Math.floor(3 * 0.5)) * boardSize * initSize * 4)
    for (var i = 0; i < quant.length; i++) {
      for (var j = 0; j < 4; j++) {
        push()
        translate(windowWidth * 0.5 + (i - Math.floor(quant.length * 0.5) + (quant.length % 2 + 1) * 0.5) * boardSize * initSize * 0.6, windowHeight * 0.5 + boardSize * initSize * 2 * sin(Math.PI * (1 / 2) * j + frameCount * 0.01 + i * 0.25 + k * Math.PI * 0.125))
        textSize(initSize * boardSize * abs(sin(Math.PI * (0.5 + 0.5 * j) + frameCount * 0.01 + i * 0.25)))
        fill(255)
        noStroke()
        text(quant[i], 0, 0)
        pop()
      }
    }
    pop()
  }
}

function windowResized() {
  if (windowWidth >= windowHeight) {
    boardSize = windowHeight - 80
  } else {
    boardSize = windowWidth - 80
  }
  createCanvas(windowWidth, windowHeight)
}

function create2DArray(numRows, numCols, init, bool) {
  var array = [];
  for (var i = 0; i < numRows; i++) {
    var columns = []
    for (var j = 0; j < numCols; j++) {
      if (bool === true) {
        columns[j] = init
      } else {
        columns[j] = j * numRows + i
      }
    }
    array[i] = columns
  }
  return array
}
